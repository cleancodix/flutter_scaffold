
import 'package:flutterscaffold/tools/text.dart';

class PixabayImageDataModel {

  PixabayImageDataModel({
    this.id,
    this.tags,
    this.previewURL,
    this.largeImageURL,
    this.views,
    this.downloads,
    this.likes,
    this.comments,
    this.user,
  });

  final int id;
  final List<String> tags;
  final String previewURL;
  final String largeImageURL;
  final int views;
  final int downloads;
  final int likes;
  final int comments;
  final String user;

  String get name => tags[0];
  String get imageHero => 'IMAGE-$id-$name';
  String get titleHero => 'TITLE-$id-$name';
  String get subtitleHero => 'SUBTITLE-$id-$name';

  static List<String> tagsFromJson(String jsonTags) => jsonTags.split(', ').map((tag) => TextTool.capitalize(tag)).toList();

  static PixabayImageDataModel fromJson(Map<String, dynamic> data) {

    if (data == null) return null;

    return PixabayImageDataModel(
      id: data["id"],
      tags: tagsFromJson(data["tags"]),
      previewURL: data["previewURL"],
      largeImageURL: data["largeImageURL"],
      views: data["views"],
      downloads: data["downloads"],
      likes: data["likes"],
      comments: data["comments"],
      user: data["user"],
    );
  }
}