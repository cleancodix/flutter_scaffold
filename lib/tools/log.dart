
import 'dart:developer';

import 'package:flutterscaffold/constants/config/environment.dart';
import 'package:flutterscaffold/tools/date.dart';
import 'package:flutterscaffold/tools/text.dart';

abstract class Log {

  static const DefaultLogSizeLimit = 2000;

  static final bool _isProd = Environment.Prod;

  static final String _header = "____________________________________________________________";
  static final String _footer = "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾";

  static print(dynamic msg, { String title, dynamic error, dynamic stackTrace = false, int limit = DefaultLogSizeLimit }) {

    if (_isProd) return;
    _log(msg.toString(), title, error, stackTrace, limit);
  }

  static String _getTitle(String title) {
    DateTime now = DateTime.now();
    String date = "${DateTool.toFrenchDate(now)} ${DateTool.toTime(now, showMs: true)}";
    title = title != null ? "${title.toUpperCase()} : " : "";
    return "$_header\n\n$title$date\n\n";
  }

  static String _getMessage(String msg, { String title, String type, int limit }) {
    msg = TextTool.limitNbOfCharacters(msg, limit);
    return "${_getTitle(title)}$type:\n$msg\n\n$_footer";
  }

  static String _getType(dynamic error, dynamic stackTrace) {
    if (error != null) return "ERROR";
    else if (stackTrace is StackTrace || (stackTrace is bool && stackTrace)) return "DEBUG";
    else return "INFO";
  }

  static String _log(String msg, [String title, dynamic _error, dynamic stackTrace = false, int limit]) {
    msg = _getMessage(msg,
        title: title,
        limit: limit,
        type: _getType(_error, stackTrace)
    );

    if (_error != null) {
      msg = error(msg, stackTrace: stackTrace, error: _error);
    }
    else if (stackTrace) msg = debug(msg, stackTrace: stackTrace);
    else msg = info(msg);

    return "${_header.substring(0, _footer.length - _header.length)}$msg";
  }

  static String info(String msg) {
    log("$msg\n");
    return msg;
  }

  static String debug(String msg, { dynamic stackTrace }) {
    log(msg, stackTrace: _getStackTrace(stackTrace));
    log("${_footer.substring(0, _header.length)}\n\n");
    return msg;
  }

  static String error(String msg, { dynamic stackTrace, dynamic error }) {
    if (_isStackTrace(stackTrace)) log(msg, error: error, stackTrace: _getStackTrace(stackTrace));
    else log(msg, error: error);
    log("${_footer.substring(0, _header.length)}\n\n");
    return "$msg\n$error\n$_footer";
  }

  static StackTrace _getStackTrace(dynamic stackTrace) => _isStackTraceType(stackTrace) ? stackTrace : StackTrace.current;
  static bool _isStackTrace(dynamic stackTrace) => _isStackTraceType(stackTrace) || (stackTrace is bool && stackTrace);
  static bool _isStackTraceType(dynamic stackTrace) => stackTrace is StackTrace;

}
