
class TextTool {

  static String capitalize(String str) => "${str[0].toUpperCase()}${str.substring(1)}";

  static String capitalizeAll(String str) => str.split(" ").reduce((a, b) => capitalize(a) + capitalize(b));

  static String getPlural(int nb) => nb > 1 ? 's' : '';

  static String removeLast(String str) => str.substring(0, str.length - 1);

  static String removeLastZeros(String str) {
    if ((str.endsWith("0") && str.contains(".")) || str.endsWith(".") || str.endsWith(",") || str.endsWith(" ")) {
      return removeLastZeros(removeLast(str));
    }
    else
      return str;
  }

  static String removeAllAfter(String str, String target, [bool include = false]) {
    if (!str.contains(target)) return str;
    return str.substring(0, str.indexOf(target) + (include ? 0 : 1));
  }

  static String removeAllAfterLast(String str, String target, [bool include = false]) {
    if (!str.contains(target)) return str;
    return str.substring(0, str.lastIndexOf(target) + (include ? 0 : 1));
  }

  static bool notNull(String str) => str != null && str.length > 0 && str != 'null';

  static String getStringBetweenTwo(String str, String str1, String str2) {
    if (str.length <= str1.length + str2.length) return null;
    final startIndex = str.indexOf(str1);
    final endIndex = str.indexOf(str2, startIndex + str1.length);
    return (startIndex < 0 || endIndex < 0) ? null : str.substring(startIndex + str1.length, endIndex);
  }

  static String replaceStringBetweenTwo(String str, String replace, String str1, String str2) {
    final startIndex = str.indexOf(str1);
    final endIndex = str.indexOf(str2, startIndex + str1.length);

    if (startIndex < 0 || endIndex < 0) return str;

    final firstPart = str.substring(0, startIndex + str1.length);
    final endPart = str.substring(endIndex, str.length);

    return firstPart + replace + endPart;
  }

  static String joinNonNullStrings(List<String> stringList) => stringList.where((str) => TextTool.notNull(str)).join((", "));

  static String limitNbOfCharacters(String str, int limit) => str.substring(0, str.length >= limit ? limit : str.length) + (str.length > limit ? "..." : "");

  static List<String> splitEveryNCharacters(String str, int n) {
    RegExp exp = RegExp(r"\d{"+"$n"+"}");
    Iterable<Match> matches = exp.allMatches(str);
    return matches.map((match) => match.group(0)).toList();
  }
}