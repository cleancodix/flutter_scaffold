
import 'dart:ui' as UI;
import 'dart:async';
import 'dart:typed_data';
import 'package:flutterscaffold/tools/text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WidgetTool {

  static Widget wrapWithHero(String heroTag, Widget child) =>
      TextTool.notNull(heroTag) ? Hero(tag: heroTag, child: child) : child;

  static Future<UI.Image> loadUiImage(String path) async {
    final ByteData data = await rootBundle.load(path);
    final Completer<UI.Image> completer = Completer();
    UI.decodeImageFromList(Uint8List.view(data.buffer), (UI.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }
}