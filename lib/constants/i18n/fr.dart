
const Map<String, String> FRENCH = {


  //--------------------- LOGIN ------------------------

  "Log in": "Se connecter",
  "Error, check your connection\nor try again later.": "Erreur, vérifiez votre connexion\nou réessayez plus tard.",

  "Remember me": "Se souvenir de moi",
  "Forgot your password ?": "Mot de passe oublié ?",

  "Reset my password": "Réinitialiser mon mot de passe",
  "Confirm your email :": "Confirmez votre email :",

  "Cancel": "Annuler",
  "Validate": "Valider",

  "A new password has been sent to you by email.": "Un nouveau mot de passe vous a été envoyé par email.",
  "The password associated with the email": "Le mot de passe associé à l'email",
  "could not be reset.": "n'a pas pu être réinitialisé.",



  //--------------------- HOME ------------------------

  "No tag selected, please add some tags by going in Menu/Settings": "Aucun mot clé sélectionné, merci d'en ajouter en allant dans Menu/Settings",



  //--------------------- PIXABAY ------------------------

  "By": "Par",
  "Pixabay's image url expired, please go back to the list image and pull to refresh.":
  "L'url de l'image Pixabay a expiré, merci de retourner à la liste d'image et d'effectuer un \"pull to refresh\" pour la rafraîchir.",
  "Tags": "Mots clés",
  "Could not launch url": "Impossible d'ouvrir la page demandée",
  "No image found with this tag": "Aucune image trouvée avec ce mot clé :/",



  //--------------------- SETTINGS ------------------------

  "Settings": "Réglages",
  "Pixabay tags": "Tags Pixabay",
  "Theme": "Thème",
  "Light": "Clair",
  "Dark": "Sombre",



  //--------------------- COMPONENTS ------------------------


  // SIDE MENU (DRAWER)

  "Log out": "Se déconnecter",


  // EMAIL TEXT INPUT

  "E-mail": "E-mail",
  "Username": "Nom d'utilisateur",
  "Invalid e-mail": "E-mail invalide",


  // PASSWORD TEXT INPUT

  "Password": "Mot de passe",
  "characters minimum": "caractères minimum",
  "Incorrect credentials": "Identifiants incorrects",



  //--------------------- TOOLS ------------------------

  // DATE

  "Tomorrow": "Demain",
  "Today": "Aujourd'hui",
  "Yesterday": "Hier",
  "at": "à",
  "day0": "Lundi",
  "day1": "Mardi",
  "day2": "Mercredi",
  "day3": "Jeudi",
  "day4": "Vendredi",
  "day5": "Samedi",
  "day6": "Dimanche",



  //--------------------- SERVICES ------------------------

  "Thank you to grant location permission to our application in your device settings": "Merci de nous autoriser à accéder à votre position dans les réglages de votre téléphone",
  "Thank you to enable location service": "Merci d'activer le service de géolocalisation de votre téléphone"


};
