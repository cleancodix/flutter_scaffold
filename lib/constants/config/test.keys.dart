///
/// This method will combine the provided feature name
/// with one or two given keys and also maybe an unique identifier
/// in order to be used as a unique key for testing
///
/// It's easier to find a specific Button just by looking for this key:
/// ```dart
/// TestKey(feature: Features.Home, key: Keys.SpecificButton);
/// ```
///
/// Ok but there is only one specific button for this route what about List Items ?
/// Don't worry if you provide the unique item id your key should be unique too :)
/// ```dart
/// TestKey(feature: Features.ItemList, key: Keys.ListItem, id: item.id);
/// ```
///
/// Example:
/// ```dart
/// Inkwell(
///   key: Key(TestKey(
///     feature: Features.Vehicles,
///     key: Keys.VehicleListItem,
///     id: vehicle.id)
///   ),
///   child: Text('Vehicle ${vehicle.name}');
/// )
/// ```
///
/// Test example:
/// ```dart
/// await driver.tap(find.byValueKey(TestKey(feature: Features.Vehicles, key: Keys.VehicleListItem, id: vehicle.id)));
/// ```
///
/// OK ok i get it but why i need it ??
///     Hmmmm, you could use directly a simple string but here you ensure that there is no difference
///     between the key used in the original code and the key used in the tests.
///

// ignore: non_constant_identifier_names
String TestKey({Features feature, Keys key, Keys key2, dynamic id}) {
  String result = feature?.toString() ?? "";
  result += key != null ? ".$key" : "";
  result += key2 != null ? ".$key2" : "";
  result += id != null ? ".$id" : "";
  return result;
}

enum Features {
  Login,
  Home,
  Settings,
  Pixabays
}

enum Keys {
  // COMMONS
  ErrorLabel,
  OkButton,
  NextButton,
  TabButton,
  SideMenuButton,
  BackgroundLoader,
  AppBarButton,
  MenuButton,
  BackButton,
  TextInput,
  ScrollView,

  // LOGIN
  UsernameTextInput,
  EmailTextInput,
  PasswordTextInput,
  RememberMeButton,

  // SIDEMENU

  LogOutButton,
  SettingsButton,

}