
import 'package:flutter/material.dart';
import 'package:flutterscaffold/features/login/login.dart';
import 'package:flutterscaffold/features/settings/settings.dart';
import 'package:flutterscaffold/models/route.data.dart';
import 'package:flutterscaffold/services/route.dart';

import '../../features/home/home.dart';
import '../../features/pixabay/details/pixabay.details.dart';
import '../../features/pixabay/list/pixabay.list.dart';


// ignore_for_file: non_constant_identifier_names
abstract class Routes {

  static Map<String, Widget Function(BuildContext)> Pages = {};
  static Map<String, Widget> ActionButtons = {};
  static Map<String, List<Guard>> Guards = {};
  static Map<String, List<Guard>> PopGuards = {};
  static Map<String, List<RouteParams>> Params = {};
  static Map<String, List<String>> Histories = {};

  static Widget _commonWrapper(BuildContext context, Widget child) {
    return WillPopScope(
      onWillPop: () async {
        RouteService(context).get.pop();
        return false;
      },
      child: child,
    );
  }

  static Widget Function(BuildContext) _render(Widget Function() renderPage) {
    return (context) => _commonWrapper(context, renderPage());
  }

  static final String Login = RouteDataModel(
    name: '/',
    widget: _render(() => LoginPage()),
  ).name;

  static final String Home = RouteDataModel(
    name: '/home',
    widget: _render(() => HomePage()),
    guards: HomePage.guards,
    onPop: HomePage.onPop
  ).name;

  static final String Details = RouteDataModel(
      name: '/details',
      widget: _render(() => PixabayDetailsPage()),
      params: [
        RouteParams.ItemId,
      ]
  ).name;

  static final String Settings = RouteDataModel(
    name: '/settings',
    widget: _render(() => SettingsPage()),
  ).name;

  static MapEntry<String, String> getPixabayRouteMap(String query) => MapEntry(query,
      RouteDataModel(
        name: '/${query.toLowerCase()}',
        widget: _render(() => PixabayListModule(query)),
        onPop: PixabayListModule.onPop
      ).name
  );

  static Map<String, String> initPixabayRoutes(List<String> queries) => Pixabay = Map
      .fromEntries(queries.map(getPixabayRouteMap));

  static Map<String, String> Pixabay = initPixabayRoutes(["Animals", "Vehicles", "Sports", "Architecture", "Video games"]);
}

enum RouteParams {
  LoggedOut,
  ItemId
}