
abstract class StorageKeys {

  static const String SaveCredentials = "Save credentials";
  static const String Email = "Email";
  static const String Password = "Password";
  static const String LastVehicleId = "Last vehicle id";
  static const String PixabayQueries = "Pixabay queries";
  static const String ThemeMode = "Theme mode";
  static const String SelectedTheme = "Selected theme";
}