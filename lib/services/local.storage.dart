
import 'package:flutterscaffold/tools/log.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {

  static const int LogNbOfCharactersLimit = 1000;

  static SharedPreferences _prefs;
  static Future<SharedPreferences> get prefs async {
    _prefs ??= await SharedPreferences.getInstance();
    return _prefs;
  }

  static Future<bool> store(String key, dynamic value) async {

    await prefs;
    Log.print("Local storing <${value.runtimeType.toString()}> at key : '$key'\n\n'${value.toString()}'", title: "LOCAL STORAGE", limit: LogNbOfCharactersLimit);

    if (value is int) return _prefs.setInt(key, value);
    else if (value is double) return _prefs.setDouble(key, value);
    else if (value is String) return _prefs.setString(key, value);
    else if (value is List<String>) return _prefs.setStringList(key, value);
    else if (value is bool) return _prefs.setBool(key, value);

    return false;
  }

  static Future<List<bool>> storeAll(Map<String, dynamic> values) {
    List<Future<bool>> futures = [];
    values.forEach((key, value) => futures.add(store(key, value)));
    return Future.wait<bool>(futures);
  }

  static Future<T> get<T>(String key) async {

    await prefs;

    T result;

    if (T == int) result = _prefs.getInt(key) as T;
    else if (T == double) result = _prefs.getDouble(key) as T;
    else if (T == String) result =  _prefs.getString(key) as T;
    else if (T.toString() == "List<String>") result = _prefs.getStringList(key) as T;
    else if (T == bool) result = _prefs.getBool(key) as T;
    else result = _prefs.get(key);

    Log.print("Retrieving <${T.toString()}> at key : '$key'\n\n'${result.toString()}'", title: "LOCAL STORAGE", limit: LogNbOfCharactersLimit);

    return result != 'null' ? result : null;
  }

  static Future<List<T>> getAll<T>(List<String> keys) {
    List<Future<T>> futures = [];
    keys.forEach((value) => futures.add(get<T>(value)));
    return Future.wait<T>(futures);
  }

  static Future<bool> remove(String key) async {
    await prefs;
    Log.print("Removing key : '$key'", title: "LOCAL STORAGE");
    return _prefs.remove(key);
  }

  static Future<List<bool>> removeAll(List<String> keys) {
    List<Future<bool>> futures = [];
    keys.forEach((value) => futures.add(remove(value)));
    return Future.wait<bool>(futures);
  }

  static Future<bool> clear() async {
    await prefs;
    return _prefs.clear();
  }
}