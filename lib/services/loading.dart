
import 'package:flutterscaffold/services/layout.dart';
import 'package:mvcprovider/mvcprovider.dart';

class LoadingService extends MVC_Notifier<LoadingService> {

  LoadingService([context]) : super(context);

  bool start(dynamic id, [bool notify = true]) {
    id = id.toString();
    if (_loading[id] != true) {
      _loading[id] = true;
      // Log.print("Start loading $id", title: "Loading:Service");
      LayoutService(context).get.shouldRotateBackground = true;
      if (notify) notifyListeners();
    }
    return _loading[id];
  }

  bool stop(dynamic id, [bool notify = true]) {
    id = id.toString();
    if (_loading[id] != false) {
      _loading[id] = false;
      // Log.print("Stop loading $id", title: "LoadingService");
      LayoutService(context).get.shouldRotateBackground = false;
      if (notify) notifyListeners();
    }
    return !_loading[id];
  }

  Map<String, bool> _loading = Map<String, bool>();
  bool loading(dynamic id) => _loading.containsKey(id.toString()) && _loading[id.toString()];
  bool get isLoading => _loading.values.contains(true);
}