
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutterscaffold/components/labels/error.label.dart';
import 'package:flutterscaffold/components/layout/custom.container.dart';
import 'package:flutterscaffold/constants/config/assets.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/tools/log.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:provider/provider.dart';

class LayoutService extends MVC_Notifier<LayoutService> {

  static final toastCharacterLimit = 100;
  FToast fToast;
  Size deviceSize;
  bool isKeyboardVisible = false;

  LayoutService([context]) : super(context) {
    fToast = FToast()..init(context);
    isKeyboardVisible = KeyboardVisibilityNotification().isKeyboardVisible;
  }

  @override
  ChangeNotifierProvider<LayoutService> get create {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        isKeyboardVisible = visible;
        notifyListeners();
      },
    );
    return super.create;
  }

  @override
  LayoutService update(BuildContext context) {
    fToast.init(context);
    return super.update(context);
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ScaffoldState get scaffold => scaffoldKey.currentState;

  Widget actionButton;
  void setActionButton(String route) {
    actionButton = Routes.ActionButtons[route] ?? null;
    notifyListeners();
  }

  bool _shouldRotateBackground = false;
  bool get shouldRotateBackground => _shouldRotateBackground;
  set shouldRotateBackground(bool value) {
    if (value != shouldRotateBackground) {
      _shouldRotateBackground = value;
    }
  }

  Widget _floatingActionButton;
  Widget get floatingActionButton => _floatingActionButton;
  set floatingActionButton(Widget value) {
    if (value != floatingActionButton) {
      _floatingActionButton = value;
      notifyListeners();
    }
  }

  Future<void> alert({String title, dynamic body, Map<String, Function> buttons, int maxBodyLines = 10, bool barrierDismissible = false}) async {

    return await showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Container(
            height: screenHeight * 1/5,
            width: screenWidth * 2/3,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  if (body is String) AutoSizeText(body, maxLines: maxBodyLines),
                  if (body is Widget) body,
                ],
              ),
            ),
          ),
          actions: buttons.keys.map((key) => TextButton(
                child: Text(key),
                onPressed: () => buttons[key](),
              )).toList()
        );
      },
    );
  }

  void showToast(dynamic toast, [ToastGravity gravity = ToastGravity.BOTTOM, int duration = 3]) {
    if (toast == null) return;
    if (toast is String) {
      Log.print("show toast: ${TextTool.limitNbOfCharacters(toast, toastCharacterLimit)}");
      toast = Padding(
        padding: ThemeSizes.paddingSymmetric[Sizes.XS][Sizes.Zero],
        child: CustomContainer(
          color: Theme.of(context).accentColor,
          child: Padding(
            padding: ThemeSizes.padding[Sizes.M],
            child: ErrorLabel(TextTool.limitNbOfCharacters(toast, toastCharacterLimit), color: ThemeColors.Light),
          ),
        ),
      );
    }

    assert(toast is Widget);

    fToast.showToast(
        child: toast,
        gravity: gravity,
        toastDuration: Duration(seconds: duration)
    );
  }

  toggleSideMenu() async {
    if (scaffoldKey.currentState.isDrawerOpen) {
      scaffoldKey.currentState.openEndDrawer();
    } else {
      scaffoldKey.currentState.openDrawer();
    }
  }

  // Adapt background image size to the device width or height depending which one is larger
  double getBackgroundScaleRatio(BuildContext _context) {
    deviceSize ??= MediaQuery.of(_context).size;
    double ratio = deviceSize.width / deviceSize.height;
    ratio = ratio < 1 ? deviceSize.height / deviceSize.width : ratio;
    return ratio + 1/4; // To get a background slightly larger than screen to avoid blank area when rotating
  }

  double get screenWidth {
    double width = MediaQuery.of(context).size.width;
    EdgeInsets padding = MediaQuery.of(context).padding;
    return width - padding.horizontal;
  }

  double get screenHeight {
    double height = MediaQuery.of(context).size.height;
    EdgeInsets padding = MediaQuery.of(context).padding;
    return height - padding.top - kToolbarHeight;
  }

  String _backgroundImagePath = Assets[Backgrounds.White];
  String get backgroundImagePath => _backgroundImagePath;
  set backgroundImagePath(String value) {
    if (value != backgroundImagePath) {
      _backgroundImagePath = value;
      notifyListeners();
    }
  }
}