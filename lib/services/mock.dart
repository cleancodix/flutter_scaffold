// ignore_for_file: non_constant_identifier_names
import 'dart:convert';
import 'package:flutterscaffold/constants/config/environment.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/mocks/pixabay.dart';
import 'package:flutterscaffold/constants/mocks/users.dart';
import 'package:flutterscaffold/models/user.data.dart';
import 'package:flutterscaffold/tools/digit.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:http/http.dart';

abstract class MockService {

  static const int MinMockDuration = 100; // in ms
  static const int MaxMockDuration = 1000; // in ms
  static const double Latitude = 50.8851467;
  static const double Longitude = 4.4186917;

  static Map<String, Map<String, dynamic> Function(String)> Paths = {
    '/login': onLogin,
    ...Map.fromEntries(Routes.Pixabay.keys.map((query) =>
      MapEntry('/?key=${Environment.PixabayApiKey}&q=$query', onPixabay))),
  };

  static Future<Response> call(String url, {Map<String, String> headers, body}) async {
    String path = url.replaceAll(RegExp(Environment.HttpEndpoint), '');
    Function(Map<String, dynamic>) _response = (data) => Response(json.encode(data), 200);

    if (Paths.containsKey(path)) {
      await Future.delayed(Duration(milliseconds: DigitTool.getRandomNumber(MaxMockDuration, MinMockDuration)));
      return _response(Paths[path](body.toString()));
    }
    throw Exception("Called path not mocked :/");
  }

  static String _getFromParams(String params, String search) {
    String found = TextTool.getStringBetweenTwo(params, '"$search":', ',');
    found ??= TextTool.getStringBetweenTwo(params, '"$search":', '}');
    found ??= TextTool.getStringBetweenTwo(params, '$search:', ',');
    found ??= TextTool.getStringBetweenTwo(params, '$search:', '}');

    if (found != null) {
      found = found.trim();
      if (found.startsWith('"') && found.endsWith('"')) {
        found = found.substring(1, found.length - 1);
        found = found.trim();
      }
    }

    return found;
  }

  static Map<String, dynamic> onLogin(String params) {
    String email = _getFromParams(params, "login");

    UserDataModel user = MockedUsers.firstWhere(
            (user) => user.email == email,
        orElse: () => null
    );

    if (user == null) return {
      "error": "Invalid credentials"
    };

    return {
      "userData": {
        "id": user.id,
        "firstName": user.firstName,
        "lastName": user.lastName,
        "email": user.email,
        "password": user.password,
        "avatarUrl": user.avatarUrl,
        "sessionToken": user.sessionToken
      },
    };
  }

  static Map<String, dynamic> onPixabay(String params) {
    String query = _getFromParams(params, "q");

    return MockedPixabay.containsKey(query) ?
      MockedPixabay[query] : null;
  }
}
