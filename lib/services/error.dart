import 'dart:async';

import 'package:flutterscaffold/constants/config/storage.keys.dart';
import 'package:flutterscaffold/models/user.data.dart';
import 'package:flutterscaffold/services/local.storage.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:flutter/material.dart';

import '../features/login/login.provider.dart';
import 'cache.dart';
import 'http.dart';
import 'layout.dart';
import 'logout.dart';

class ErrorService {

  static const int LogNbOfCharactersLimit = 100;

  static Map<String, Function> errorHandlers = {
    "Your session has been interrupted": _onSessionExpired,
    "Invalid credentials": _onInvalidCredentials,
  };

  static void defaultErrorHandler(BuildContext context, String error) {
      String serverError = TextTool.getStringBetweenTwo(error, "<p><b>description</b> <u>", "</u></p>");
      error = serverError ?? TextTool.limitNbOfCharacters(error, LogNbOfCharactersLimit);
      LayoutService(context).get.showToast(error);
  }

  static void _onInvalidCredentials(HttpRequest request, String error) async {
    LayoutService(request.context).get.showToast(request.error);
    return null;
  }

  static Future<dynamic> _onSessionExpired(HttpRequest request, String error) async {
    bool exit = true;

    if (await LocalStorageService.get<bool>(StorageKeys.SaveCredentials)) {
      String username =
      await LocalStorageService.get<String>(StorageKeys.Email);
      String password =
      await LocalStorageService.get<String>(StorageKeys.Password);
      UserDataModel user =
      await LoginProvider(request.context).login(username, password);
      if (user != null) {
        exit = false;
        CacheService(request.context).get.loggedUser.sessionToken = user.sessionToken;
        request.params["sessionId"] = user.sessionToken;
        request.error = null;
      }
    }
    if (exit) LogoutService.exit(request.context);
    else return await request.run();
  }
}