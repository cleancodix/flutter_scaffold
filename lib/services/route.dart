// ignore_for_file: non_constant_identifier_names
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/services/guard.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/log.dart';
import 'package:mvcprovider/mvcprovider.dart';

class RouteService extends MVC_Provider<RouteService> {

  RouteService([context]) : super(context);

  static final String Initial = Routes.Login;
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static List<String> History = [Initial];

  NavigatorState get _navigator => navigatorKey.currentState;
  bool get isParams => ModalRoute.of(context).settings.arguments is Map<RouteParams, dynamic>;
  Map<RouteParams, dynamic> get params => isParams ? ModalRoute.of(context).settings.arguments : {};

  Future<bool> _douane(Destination destination) async {
    if (Routes.Guards.containsKey(destination.next)) {
      Destination originalDestination = Destination(destination.next, destination.params);

      if (!await _executeGuards(GuardService.goToStartingGuards, destination))
        return false;

      if (!await _executeGuards(Routes.Guards[destination.next], destination))
        return false;

      if (originalDestination.next != destination.next) {
        if (!await _executeGuards(GuardService.goToEndingGuards, originalDestination))
          return false;

        if (!await _douane(destination))
          return false;
      }
      else if (!await _executeGuards(GuardService.goToEndingGuards, destination))
        return false;
    }
    return true;
  }

  Future<bool> _executeGuards(List<Guard> guards, Destination destination) async {
    bool result = true;
    try {
      await Future.forEach(guards, (guard) async {
        result = await guard(context, destination);
        if (!result) throw Exception("\n\n"
            "         \"You shall not pass !\"\n\n"
            "                        - Gandalf -\n"
        );
      });
      return result;
    } catch (e, stackTrace) {
      Log.print('Error while executing "${destination.next}" guards', title: 'RouteService', error: e, stackTrace: result ? stackTrace : null);
      return false;
    }
  }

  Future<dynamic> goTo(String route, {Map<RouteParams, dynamic> params, bool replace = false, bool removeUntil = false}) async {
    Function navigate = (_route, _params) => _navigator.pushNamed(_route, arguments: _params);
    if (removeUntil) navigate = (_route, _params) => _navigator.pushNamedAndRemoveUntil(_route, (_) => false, arguments: _params);
    else if (replace) navigate = (_route, _params) => _navigator.pushReplacementNamed(_route, arguments: _params);

    Destination destination = Destination(route, params ?? {});

    if (await _douane(destination)) {
      updateHistory(destination.next, removeUntil: removeUntil, replace: replace);
      return navigate(destination.next, destination.params)
          .then((popParams) => onPop(destination, popParams));
    }
    // Because even if guard aren't passed we want to stop the loading animation
    else stopLoading(route, destination);
    return null;
  }

  void stopLoading(String route, Destination destination) {
    GuardService.stopLoading(context, destination);
    if (destination.next != route && LoadingService(context).get.loading(route)) {
      // Because destination.next is used as loadingId and it may be changed during guards
      destination.next = route;
      GuardService.stopLoading(context, destination);
    }
  }

  void pop([dynamic params]) {
    if (_navigator.canPop()) {
      if (History.isNotEmpty && Routes.Histories[History?.last] != null) {
        _navigator.popUntil((route) {
          return route.settings.name == Routes.Histories[History.last]?.last;
        });
      }
      else _navigator.pop(PopParams(context, params));
    }
  }

  dynamic onPop(Destination destination, dynamic popParams) async {
    if (popParams is PopParams) {
      String previous = destination.previous;
      destination.previous = destination.next;
      destination.next = previous;
      await Future.forEach(
          GuardService.poppingGuards,
              (guard) async => await guard(context, destination)
      );
      if (Routes.PopGuards.containsKey(destination.next)) {
        await _executeGuards(Routes.PopGuards[destination.next], destination);
      }
    }
    updateHistory(null);
    return popParams?.data ?? popParams;
  }

  void updateHistory(String nextRoute, { bool replace = false, bool removeUntil = false}) {
    if (nextRoute != null) {
      if (Routes.Histories[nextRoute] != null) {
        History = [...Routes.Histories[nextRoute]];
      }
      else if (removeUntil) History = [];
      else if (replace) History.removeLast();
      History.add(nextRoute);
    }
    else History.removeLast();
  }
}

class PopParams {
  final BuildContext context;
  dynamic data;
  PopParams(this.context, this.data);
}

class Destination {

  String next;
  Map<RouteParams, dynamic> params;
  String previous;

  Destination([this.next, this.params]) : this.previous = RouteService.History.isNotEmpty ? RouteService.History?.last : null;
}

typedef Guard = Future<bool> Function(BuildContext, Destination);
