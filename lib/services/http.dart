// ignore_for_file: non_constant_identifier_names
import 'dart:convert';
import 'package:flutterscaffold/services/mock.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutterscaffold/constants/config/environment.dart';
import 'package:flutterscaffold/services/layout.dart';
import 'package:flutterscaffold/services/local.storage.dart';
import 'package:flutterscaffold/tools/log.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'error.dart';

class HttpService {

  BuildContext context;

  HttpService(this.context);

  static final Client _client = Client();

  static final int Timeout = 30; // In seconds

  static final Map<HTTPMethods, Function> Call = {
    HTTPMethods.POST: _client.post,
    HTTPMethods.GET: (url, {Map<String, String> headers, body}) => _client.get(url, headers: headers),
    HTTPMethods.PUT: _client.put,
    HTTPMethods.DELETE: (url, {Map<String, String> headers, body}) => _client.delete(url, headers: headers),
    HTTPMethods.MOCK: MockService.call
  };

  Future<T> get<T>({
    String url,
    String path,
    Map<String, dynamic> params,
    Map<String, String> headers,
    T Function(Map<String, dynamic>) factory,
    Future<T> Function(Map<String, dynamic>) asyncFactory,
    HTTPMethods method = HTTPMethods.POST,
    String responseProperty,
    bool paramsAsString = false,
    bool force = true,
    Duration cacheExpiration,
    Function onFail,
    Function(Response) onResponse,
    Function(T) onSuccess,
  }) async {

    HttpRequest request = HttpRequest<Map<String, dynamic>>();
    bool isFactory = asyncFactory != null || factory != null;
    bool isPrimitiveType = !isFactory && PrimitivesTypes.contains(T);
    if (isPrimitiveType) request = HttpRequest<T>();

    request
      ..url = url
      ..path = path
      ..params = params
      ..paramsAsString = paramsAsString
      ..headers = headers
      ..method = method
      ..responseProperty = responseProperty
      ..force = force
      ..cacheExpiration = cacheExpiration
      ..isPrimitive = isPrimitiveType
      ..onFail = onFail
      ..onResponse = onResponse
      ..context = context;

    request.init();
    dynamic parsedResponse = await request.run();

    try {
      T result;
      if (parsedResponse != null) {
        if (asyncFactory != null) result = await asyncFactory(parsedResponse);
        else if (factory != null) result = factory(parsedResponse);
        else result = parsedResponse;
      }
      if (onSuccess != null) await onSuccess(result);
      return result;
    } catch(e) {
      Log.print("Http request error while executing ${T.toString()} factory", title: 'HttpService', error: e, stackTrace: true);
      return null;
    }
  }

  Future<List<T>> getList<T>({
    String url,
    String path,
    Map<String, dynamic> params,
    Map<String, String> headers,
    T Function(Map<String, dynamic>) factory,
    Future<T> Function(Map<String, dynamic>) asyncFactory,
    HTTPMethods method = HTTPMethods.POST,
    String responseProperty,
    bool paramsAsString = false,
    bool force = true,
    Duration cacheExpiration,
    Function onFail,
    Function(Response) onResponse,
    Function(List<T>) onSuccess,
  }) async {

    List<dynamic> parsedResponse = await HttpRequest<List<dynamic>>(
      url: url,
      path: path,
      params: params,
      paramsAsString: paramsAsString,
      headers: headers,
      method: method,
      responseProperty: responseProperty,
      force: force,
      cacheExpiration: cacheExpiration,
      onFail: onFail,
      onResponse: onResponse,
      context: context,
    ).run();

    try {
      List<T> result;
      if (parsedResponse != null) {
        if (asyncFactory != null) {
          result = [];
          await Future.forEach(parsedResponse, (data) async => result.add(await asyncFactory(data)));
        }
        else if (factory != null) result = parsedResponse.map((data) => factory(data)).toList();
        else result = parsedResponse;
      }
      if (onSuccess != null) await onSuccess(result);
      return result;
    } catch(e) {
      Log.print("Request error while executing List<${T.toString()}> factory", title: 'HttpService', error: e);
      return null;
    }
  }

  static List<Type> PrimitivesTypes = [
    String, int, bool, double
  ];
}

class HttpRequest<T> {

  String url;
  String path;
  Map<String, dynamic> params;
  Map<String, String> headers;
  Future<T> Function<T>(Response response, [String responseProperty]) parser;
  HTTPMethods method = HTTPMethods.POST;
  String responseProperty;
  bool isPrimitive;
  bool paramsAsString;
  bool force;
  Duration cacheExpiration;
  Function onFail;
  Function(Response) onResponse;
  BuildContext context;

  HttpRequest({
    this.url,
    this.path,
    this.params,
    this.headers,
    this.parser,
    this.method,
    this.responseProperty,
    this.isPrimitive = false,
    this.paramsAsString = false,
    this.force = true,
    this.cacheExpiration,
    this.onFail,
    this.onResponse,
    this.context
  }) {
    init();
  }

  void init() {
    url ??= Environment.HttpEndpoint;
    path ??= "";
    params ??= const {};
    parser ??= _defaultParser;
    path += method == HTTPMethods.GET ? _addGetParams(params) : "";
  }

  T result;
  String stored;
  String error;

  static const int LogNbOfCharactersLimit = 1000;
  bool get isMock => Environment.Mock || method == HTTPMethods.MOCK;
  String get logTitle => "${isMock ? "MOCKED " : ""}HttpService";
  String get dataStorageKey => "${_storageKeyFormatter()}-DATA";
  String get lastSyncDateStorageKey => "${_storageKeyFormatter()}-LASTSYNCDATE";
  Future<bool> get isExpired async {
    if (cacheExpiration == null) return true;
    String lastSyncDate = await LocalStorageService.get(lastSyncDateStorageKey);
    if (lastSyncDate == null) return true;
    return DateTime.now().difference(DateTime.parse(lastSyncDate)).compareTo(cacheExpiration) > 0;
  }

  Future<T> run() async {
    try {
      if (!await isExpired && !force) stored = await LocalStorageService.get<String>(dataStorageKey);
      if (stored != null) result = json.decode(stored);
      else {
        if (Environment.Mock || method == HTTPMethods.MOCK) {
          method = HTTPMethods.MOCK;
        }
        Log.print("Requesting ${T.toString()} : $url$path,\n\nparams: ${params.toString()},\n\nheaders : ${headers.toString()}", title: logTitle);

        Response response = await HttpService.Call[method](url + path, body: paramsAsString ? json.encode(params) : params, headers: headers, )
            .timeout(Duration(seconds: HttpService.Timeout), onTimeout: () {
          Log.print("Failed requesting <${T.toString()}>", title: logTitle, error: "Request timeout");
          LayoutService(context).get.showToast("Request canceled because too long. Try again later please.");
          return null;
        });

        if (response != null) {
          result = await parser<T>(response, responseProperty);
          if (onResponse != null) await onResponse(response);
        }
      }
    }
    catch(e) {
      String error = e.toString().replaceFirst("Exception: ", "");
      if (ErrorService.errorHandlers.containsKey(error)) {
        return await ErrorService.errorHandlers[error](this, error);
      }
      Log.print("Request error while requesting or parsing <${T.toString()}>", title: logTitle, error: error);
      ErrorService.defaultErrorHandler(context, error);
      if (onFail != null) onFail();
      return null;
    }

    if (result != null && cacheExpiration != null && (force || stored == null)) {
      await LocalStorageService.store(dataStorageKey, json.encode(result));
      await LocalStorageService.store(lastSyncDateStorageKey, DateTime.now().toString());
    }

    return result;
  }

  // Transform POST params into GET url params
  String _addGetParams(Map<String, dynamic> params) {
    if (params.length == 0) return "";
    String result = "?";
    params.forEach((key, value) {
      result += "$key=$value&";
    });
    return result.substring(0, result.length - 1);
  }

  // Transform an object Response into the given type
  Future<T> _defaultParser<T>(Response response, [String responseProperty]) async {
    dynamic result;
    try {
      if (_isEmpty(response)) return result;
      Log.print("Response status: ${response?.statusCode}\nParsing ${T.toString()}:\n\n${response.body.toString()}", title: logTitle, limit: LogNbOfCharactersLimit);
      result = json.decode(response.body);
      result = responseProperty != null ? result[responseProperty] : result;
      if (isPrimitive) result = _primitiveTypeParser<T>(result);
    } catch(e) {
      Log.print("Request error while parsing response", title: logTitle, error: e, stackTrace: true);
      if (TextTool.notNull(response?.body?.toString())) throw Exception(response.body.toString());
      return null;
    }
    handleResponseError(response, result);
    return result as T;
  }

  T _primitiveTypeParser<T>(dynamic body) {

    switch (T) {
      case bool:
        return (body is String && TextTool.notNull(body) && body != "null"
            && body != "false" || body is int && body >= 0) as T;
      case double: return double.parse(body) as T;
      case int: return double.parse(body).toInt() as T;
      case String: return (body is String ? body : json.encode(body)) as T;
      default: return body as T;
    }
  }

  // Throw and exception with an error message if there is an error
  void handleResponseError(Response response, [dynamic parsedResponse]) {
    parsedResponse ??= response?.body;

    if (parsedResponse != null && !(parsedResponse is List<dynamic>)) {

      try {
        parsedResponse = parsedResponse is String
            ? json.decode(parsedResponse) : parsedResponse as Map<String, dynamic>;
      } catch(e) {
        parsedResponse = response.statusCode != 200 ? { "error": parsedResponse.toString() } : null;
      }

      if (response.statusCode != 200 || (parsedResponse != null
          && (parsedResponse["error"] != null
              || parsedResponse["errorMessage"] != null))) {
        // Check server message error
        if (parsedResponse != null) {
          if (parsedResponse["error"] != null) {
            error = "${parsedResponse["error"]}";
          }
          if (parsedResponse["errorMessage"] != null) {
            error = "${parsedResponse["errorMessage"]}";
          }
          if (error != null) {
            if (parsedResponse["errorCode"] != null) {
              error = "${parsedResponse["errorCode"]} - $error";
            }
          }
          else error = "Error : ${response.body}";
        }
        else error = "Error : ${response.body}";
      }
    } else if (response?.statusCode != 200) {
      error = "Response status ${response.statusCode}";
    }

    if (TextTool.notNull(error)) throw Exception(error);
  }

  String _storageKeyFormatter() {
    String result = "$url$path";
    Map<String, dynamic> _params = { ...params };
    // Do something to adapt storage key to cache the http response
    // Like removing the session token maybe ?
    return "$result${_params.toString()}";
  }

  bool _isEmpty(dynamic response) => response == null || response.body == null || (response.body != null && response.body.length == 0);

}

enum HTTPMethods { POST, GET, PUT, DELETE, MOCK }
