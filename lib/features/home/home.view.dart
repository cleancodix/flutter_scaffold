
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/components/layout/custom.sliver.bar.dart';
import 'package:flutterscaffold/components/layout/sliver.view.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';

import '../../components/images/main.logo.dart';
import 'home.ctrl.dart';
import 'home.model.dart';

class HomeView extends StatefulWidget  {

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with MVC_View<HomeModel, HomeCtrl>, TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    listen(context);

    MainLogo mainLogo = MainLogo();

    TabController tabController = TabController(
        initialIndex: model.currentTabIndex,
        length: Routes.Pixabay.keys.length,
        vsync: this
    );

    Widget mainContent = model.tabViews.length > 0 ? TabBarView(
        controller: tabController,
        children: model.tabViews
    ): Center(
      child: Padding(
        padding: ThemeSizes.padding[Sizes.L],
        child: SubtitleLabel(
          TranslationService(context).get["No tag selected, please add some tags by going in Menu/Settings"],
          maxLines: 3,
        ),
      ),
    );

    return SliverView(
      nested: true,
      appBar: CustomSliverBar(
            background: mainLogo,
            expandedHeight: mainLogo.height,
            collapsed: TabBar(
              controller: tabController,
              isScrollable: true,
              onTap: ctrl.onTabTap,
              tabs: Routes.Pixabay.keys.map((key) => Hero(
                tag: "TITLE-$key",
                child: Tab(text: key),
              )).toList(),
              labelColor: Theme.of(context).colorScheme.onPrimary,
            ),
            padding: ThemeSizes.padding[Sizes.Zero],
        ),
        child: mainContent
    );
  }
}