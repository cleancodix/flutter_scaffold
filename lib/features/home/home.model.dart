
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/features/home/home.ctrl.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:mvcprovider/mvcprovider.dart';

class HomeModel extends MVC_Model<HomeCtrl> {

  List<Widget> tabViews;
  int currentTabIndex = 0;
  CacheService get cache => CacheService(context).get;

  void initTabs([bool shouldRefresh = false]) {
    tabViews = Routes.Pixabay.values.map((query) => Routes.Pages[query](context)).toList();
    if (shouldRefresh) {
      currentTabIndex = currentTabIndex >= tabViews.length ? 0 : currentTabIndex;
      notifyListeners();
    }
  }

  @override
  void init() => initTabs();
}