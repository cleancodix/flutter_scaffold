
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'home.model.dart';

class HomeCtrl extends MVC_Controller<HomeModel> {

  void onTabTap(int i) {
    if (i != model.currentTabIndex) model.currentTabIndex = i;
    else {
      RouteService(context).get.goTo(Routes.Pixabay.values.toList()[i]);
    }
  }
}