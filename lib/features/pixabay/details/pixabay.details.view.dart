
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/labels/custom.label.dart';
import 'package:flutterscaffold/components/labels/simple.label.dart';
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/components/labels/title.label.dart';
import 'package:flutterscaffold/components/layout/custom.sliver.bar.dart';
import 'package:flutterscaffold/components/layout/sliver.view.dart';
import 'package:flutterscaffold/components/misc/measure.size.dart';
import 'package:flutterscaffold/components/misc/rotate.on.loading.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/constants/theme/theme.fonts.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'pixabay.details.ctrl.dart';
import 'pixabay.details.model.dart';

class PixabayDetailsView extends StatelessWidget with MVC_View<PixabayDetailsModel, PixabayDetailsCtrl> {

  @override
  Widget build(BuildContext context) {
    listen(context);

    Widget body = Padding(
      padding: ThemeSizes.padding[Sizes.M],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
            child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Hero(
                        tag: 'TITLE-${model.image.id}-${model.image.name}',
                        child: TitleLabel(model.image.name)
                    ),
                    Hero(
                        tag: 'SUBTITLE-${model.image.id}-${model.image.name}',
                        child: SubtitleLabel("${TranslationService(context).get["By"]}: ${model.image.user}")
                    ),
                Padding(
                  padding: ThemeSizes.paddingOnly[AxisDirection.up][Sizes.L],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Padding(
                              padding: ThemeSizes.paddingOnly[AxisDirection.right][Sizes.XS],
                              child: SimpleLabel(model.image.likes.toString()),
                            ),
                            Icon(Icons.favorite, color: Colors.red),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Padding(
                              padding: ThemeSizes.paddingOnly[AxisDirection.right][Sizes.XS],
                              child: SimpleLabel(model.image.downloads.toString()),
                            ),
                            Icon(Icons.file_download, color: ThemeColors.MediumGrey),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Padding(
                              padding: ThemeSizes.paddingOnly[AxisDirection.right][Sizes.XS],
                              child: SimpleLabel(model.image.comments.toString()),
                            ),
                            Icon(Icons.message, color: Colors.blue),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SimpleLabel("${TranslationService(context).get["Tags"]} :", size: Sizes.L),
                Wrap(
                    children: model.image.tags.map((tag) {
                      return Padding(
                        padding: ThemeSizes.padding[Sizes.XXS],
                        child: Chip(
                          label: Text(tag),
                        ),
                      );
                    }).toList()
                ),
              ],
            ),
          ),
          InkWell(
            onTap: ctrl.launchUrl,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SimpleLabel("URL :", size: Sizes.L),
                Padding(
                  padding: ThemeSizes.padding[Sizes.XXS],
                  child: CustomLabel(
                    model.image.largeImageURL,
                    style: ThemeFonts.Link,
                    maxLines: 2,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );

    return SliverView(
        appBar: CustomSliverBar(
            title: Hero(
              tag: "IMAGE-${model.image.id}-${model.image.name}",
              child: MeasureSize(
                onChange: model.setImageHeight,
                child: Image.network(
                  model.image.largeImageURL,
                  fit: BoxFit.fitWidth,
                  frameBuilder: (context, child, i, loaded) {
                    if (i == null) return Padding(
                      padding: ThemeSizes.padding[Sizes.XS],
                      child: RotateOnLoading(
                        force: true,
                        child: CircleAvatar(
                            backgroundImage: NetworkImage(model.image.previewURL)
                        ),
                      ),
                    );
                    model.imageLoaded = true;
                    return child;
                  },
                ),
              ),
            ),
            padding: ThemeSizes.padding[Sizes.Zero],
            expandedHeight: model.imageHeight,
          ),
        child: body
    );
  }
}