
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/layout.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:flutterscaffold/tools/log.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'pixabay.details.model.dart';

class PixabayDetailsCtrl extends MVC_Controller<PixabayDetailsModel> {

  LayoutService get layout => LayoutService(context).get;
  Map<String, String> get translate => TranslationService(context).get;

  // Called one time before first view rendering
  @override
  void init() {
    int imageId = RouteService(context).get.params[RouteParams.ItemId];
    model.image = CacheService(context).get.images[imageId];
  }

  launchUrl() async {
    String url = model.image.largeImageURL;
    try {
      if (await canLaunch(url)) await launch(url);
      else
        throw Exception("url_launcher.canLaunch() is false");
    } catch(e) {
      Log.print("Can't launch: $url", error: e);
      layout.showToast("${translate["Could not launch url"]}: $url");
    }
  }
}