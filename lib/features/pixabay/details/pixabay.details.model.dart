
import 'package:flutter/material.dart';
import 'package:flutterscaffold/models/pixabay.image.data.dart';
import 'package:mvcprovider/mvcprovider.dart';

class PixabayDetailsModel extends MVC_Model {

  bool imageLoaded = false;
  double imageHeight = kToolbarHeight;
  void setImageHeight(Size imageSize) {
    if (!imageLoaded) return;
    Size deviceSize = MediaQuery.of(context).size;
    double ratio = imageSize.width / imageSize.height;
    double result = deviceSize.width / ratio;

    if (imageHeight != result) {
      imageHeight = result;
      notifyListeners();
    }
  }

  PixabayImageDataModel image;
}