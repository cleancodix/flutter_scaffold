
import 'package:mvcprovider/mvcprovider.dart';

import 'pixabay.details.ctrl.dart';
import 'pixabay.details.model.dart';
import 'pixabay.details.view.dart';

class PixabayDetailsPage extends MVC_Module<PixabayDetailsModel, PixabayDetailsView, PixabayDetailsCtrl> {

  final PixabayDetailsModel model = PixabayDetailsModel();
  final PixabayDetailsView view = PixabayDetailsView();
  final PixabayDetailsCtrl ctrl = PixabayDetailsCtrl();
}