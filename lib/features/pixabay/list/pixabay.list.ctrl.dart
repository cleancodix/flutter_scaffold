
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutterscaffold/tools/log.dart';
import 'package:mvcprovider/mvcprovider.dart';

import '../pixabay.provider.dart';
import 'pixabay.list.model.dart';

class PixabayListCtrl extends MVC_Controller<PixabayListModel> {

  CacheService get cacheService => CacheService(context).get;
  LoadingService get loading => LoadingService(context).get;
  GlobalKey<RefreshIndicatorState> refreshKey = GlobalKey<RefreshIndicatorState>();

  // Called one time after first view rendering
  @override
  void ready() {
    requestImages();
  }

  Future<void> requestImages([bool force = false]) async {
    loading.start(model.loadingId);
    refreshKey.currentState.show();
    try {
      Log.print('Retrieving ${model.query} images list', title: 'PixabayListCtrl');
      model.images = await PixabayProvider(context).getImageList(model.query, force);
      if (model.images != null) {
        cacheService.setImages(model.query, model.images);
        notifyListeners();
      }
      loading.stop(model.loadingId);
    } catch(e) {
      Log.print('Error while retrieving "${model.query}" images', title: 'PixabayService', error: e);
    }
    loading.stop(model.loadingId);
  }
}