
import 'package:flutterscaffold/services/route.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'pixabay.list.ctrl.dart';
import 'pixabay.list.model.dart';
import 'pixabay.list.view.dart';

class PixabayListModule extends MVC_Module<PixabayListModel, PixabayListView, PixabayListCtrl> {

  static PixabayListModel _model;
  final PixabayListModel model;
  final PixabayListView view = PixabayListView();
  final PixabayListCtrl ctrl = PixabayListCtrl();

  PixabayListModule(String query) : model = PixabayListModel(query) {
    _model = model;
  }

  static final List<Guard> onPop = [
    (_context, destination) async {
      _model.initAppBar(true);
      return true;
    },
  ];
}