
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/labels/custom.label.dart';
import 'package:flutterscaffold/components/labels/title.label.dart';
import 'package:flutterscaffold/components/layout/custom.container.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/models/pixabay.image.data.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:flutterscaffold/services/translation.dart';

class PixabayListItem extends StatelessWidget {

  final PixabayImageDataModel image;

  PixabayListItem([this.image]);

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: ThemeSizes.padding[Sizes.M],
      child: CustomContainer(
        child: ListTile(
          onTap: () => RouteService(context).goTo(Routes.Details, params: {
            RouteParams.ItemId: image.id
          }),
          leading: Hero(
            tag: 'IMAGE-${image.id}-${image.name}',
            child: CircleAvatar(
                backgroundImage: NetworkImage(image.previewURL)
            ),
          ),
          title: Hero(
              tag: 'TITLE-${image.id}-${image.name}',
              child: TitleLabel(image.name, size: Sizes.S)
          ),
          subtitle: Hero(
              tag: 'SUBTITLE-${image.id}-${image.name}',
              child: CustomLabel("${TranslationService(context).get["By"]}: ${image.user}", style: Theme.of(context).textTheme.overline)
          ),
        ),
      ),
    );
  }



}