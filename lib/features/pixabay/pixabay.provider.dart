
import 'package:flutterscaffold/constants/config/environment.dart';
import 'package:flutterscaffold/models/pixabay.image.data.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/http.dart';

class PixabayProvider extends HttpService {
  static final path = "/";

  PixabayProvider(context) : super(context);

  CacheService get cacheService => CacheService(context).get;

  // Should return a list of pixabay images data
  Future<List<PixabayImageDataModel>> getImageList(String query, bool force) async => await this.getList<PixabayImageDataModel>(
      path: path,
      params: {
        "key": Environment.PixabayApiKey,
        "q": query
      },
      factory: PixabayImageDataModel.fromJson,
      method: HTTPMethods.GET,
      responseProperty: 'hits',
      force: force,
      cacheExpiration: Duration(minutes: 30),
  );
}
