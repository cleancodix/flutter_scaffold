
import 'package:flutterscaffold/components/inputs/custom.text.input.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/features/settings/components/query.text.input.dart';
import 'package:flutterscaffold/features/settings/settings.ctrl.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';

class SettingsModel extends MVC_Model<SettingsCtrl> {

  String errorMsg;
  String get title => translate["Settings"];
  Map<String, String> get translate => TranslationService(context).get;
  CacheService get cache => CacheService(context).get;

  List<String> queries;
  CustomTextInputModel tagInput;
  Features loadingId = Features.Settings;
  Map<String, String> themes;

  @override
  void init() {
    queries = Routes.Pixabay.values.toList();
    tagInput = QueryTextInputModel(next: () => ctrl.formKey.currentState.submit());
    themes = Map.fromEntries(ThemeColors.Switcher.keys.map((key) => MapEntry(key, key)));
  }
}