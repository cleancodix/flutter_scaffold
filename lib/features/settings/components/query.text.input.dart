
import 'package:flutterscaffold/components/inputs/custom.text.input.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/translation.dart';

class QueryTextInput extends StatelessWidget {

  QueryTextInput({ this.model });

  final QueryTextInputModel model;
  final String testKey = TestKey(feature: Features.Settings, key: Keys.TextInput);

  @override
  Widget build(BuildContext context) {
    QueryTextInputModel _model = model ?? QueryTextInput();
    return CustomTextInput(model: _model, testKey: testKey);
  }
}

class QueryTextInputModel extends CustomTextInputModel {

  QueryTextInputModel({String value, dynamic next}) : super(value: value, next: next);

  int minLength = 3;
  Map<String, String> get translate => TranslationService(context).get;

  @override
  String get label => translate["Pixabay tags"];
  @override
  String get invalidMessage => minLength.toString() + " " + translate["characters minimum"];
  @override
  String get errorMessage => "Omfg";

  final bool required = true;
  final Icon icon = Icon(Icons.tag, size: ThemeSizes.icon[Sizes.M]);
  final TextAlign textAlign = TextAlign.center;

  @override
  bool validate() => super.validate() && value.length >= minLength;
}