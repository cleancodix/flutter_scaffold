
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/buttons/ok.button.dart';
import 'package:flutterscaffold/components/images/main.logo.dart';
import 'package:flutterscaffold/components/inputs/custom.form.dart';
import 'package:flutterscaffold/components/inputs/email.text.input.dart';
import 'package:flutterscaffold/components/inputs/password.text.input.dart';
import 'package:flutterscaffold/components/labels/error.label.dart';
import 'package:flutterscaffold/components/layout/paddings.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';

import '../../components/labels/simple.label.dart';
import 'login.ctrl.dart';
import 'login.model.dart';

class LoginView extends StatelessWidget with MVC_View<LoginModel, LoginCtrl> {

  @override
  Widget build(BuildContext context) {
    listen(context);
    bool isLoading = LoadingService(context).listen.isLoading;

    return SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Padding(
              padding: ThemeSizes.paddingOnly[AxisDirection.up][Sizes.XL],
              child: MainLogo(axis: Axis.vertical),
            ),
            Padding(
              padding: ThemeSizes.paddingSymmetric[Sizes.S][Sizes.XXS],
              child: CustomForm(
                key: ctrl.formKey,
                onSubmit: ctrl.onSubmit,
                onSubmitFail: ctrl.onLoginFail,
                disableSubmit: isLoading,
                inputModels: [
                  model.email,
                  model.password
                ],
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: ThemeSizes.paddingSymmetric[Sizes.M]
                        [Sizes.Zero],
                        child: Column(
                          children: <Widget>[
                            Paddings(
                              paddings: [
                                ThemeSizes.paddingOnly[AxisDirection.down][Sizes.S],
                                ThemeSizes.paddingOnly[AxisDirection.up][Sizes.M]
                              ],
                              child: EmailTextInput(model: model.email, testKey: TestKey(feature: Features.Login, key: Keys.EmailTextInput)),
                            ),
                            PasswordTextInput(model: model.password, testKey: TestKey(feature: Features.Login, key: Keys.PasswordTextInput)),
                            if (model.errorMsg != null)
                              ErrorLabel(model.errorMsg, testKey: TestKey(feature: Features.Login, key: Keys.ErrorLabel))
                          ],
                        ),
                      ),
                      Paddings(
                          paddings: [
                            ThemeSizes.paddingOnly[AxisDirection.up][Sizes.Zero],
                            ThemeSizes.paddingOnly[AxisDirection.down][Sizes.S],
                            ThemeSizes.paddingSymmetric[Sizes.M][Sizes.Zero]
                          ],
                          child: InkWell(
                              key: Key(TestKey(feature: Features.Login, key: Keys.RememberMeButton)),
                              onTap: model.isPasswordError
                                  ? ctrl.showForgotPasswordDialog
                                  : () => model.setWantToSaveCredential(!model.wantToSaveCredential),
                              child: model.isPasswordError
                                  ? Center(
                                  child: Padding(
                                    padding: ThemeSizes.padding[Sizes.S],
                                    child: SimpleLabel(TranslationService(context).get["Forgot your password ?"]),
                                  ))
                                  : Stack(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Checkbox(
                                      value: model.wantToSaveCredential,
                                      onChanged: model.setWantToSaveCredential,
                                    ),
                                  ),
                                  Positioned.fill(
                                      child: Center(
                                          child: SimpleLabel(
                                              TranslationService(context).get["Remember me"]))),
                                ],
                              ))),
                      Padding(
                        padding: ThemeSizes.padding[Sizes.M],
                        child: OkButton(
                            action: ctrl.submit,
                            testKey: TestKey(feature: Features.Login, key: Keys.OkButton),
                            disabled: isLoading,
                        ),
                      ),
                    ]),
              ),
            ),
          ]),
        ));
  }
}
