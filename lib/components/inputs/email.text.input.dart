
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/regex.dart';

import 'custom.text.input.dart';

class EmailTextInput extends StatelessWidget {

  EmailTextInput({ model, this.testKey })
      : this.model = model ?? EmailTextInputModel();

  final EmailTextInputModel model;
  final String testKey;

  @override
  Widget build(BuildContext context) {
    return CustomTextInput(model: model, testKey: testKey);
  }
}

class EmailTextInputModel extends CustomTextInputModel {

  EmailTextInputModel({String value, dynamic next})
      : super(value: value, next: next);

  @override
  String get label => translate["E-mail"];
  @override
  String get invalidMessage => translate["Invalid e-mail"];
  @override
  String get errorMessage => translate["Incorrect credentials"];

  final bool secret = false;
  final bool required = true;
  final int maxLines = 1;
  final TextInputType keyboard = TextInputType.emailAddress;
  final Icon icon = Icon(Icons.mail, size: ThemeSizes.icon[Sizes.M]);
  final TextAlign textAlign = TextAlign.center;

  @override
  bool validate() => super.validate() && Regex.email.hasMatch(value);
}