
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';

class SelectBox<T> extends StatefulWidget {

  final Map<String, T> keyValues;
  final T defaultValue;
  final Function(T) onChanged;
  final IconData icon;

  SelectBox({
    @required this.keyValues,
    @required this.onChanged,
    this.defaultValue,
    this.icon
  });

  @override
  _SelectBoxState createState() => _SelectBoxState<T>();
}

class _SelectBoxState<T> extends State<SelectBox> {

  T _selected;

  T _onChanged(T value) {
    setState(() {});
    _selected = value;
    widget.onChanged(value);
    return value;
  }

  List<DropdownMenuItem<T>> get _typeSelectItems => widget.keyValues.keys.map((key) {
    return DropdownMenuItem<T>(
      child: SubtitleLabel(
        widget.keyValues[key],
        size: Sizes.S,
        align: TextAlign.end,
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        expand: true,
      ),
      value: key as T,
    );
  }).toList();

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      alignedDropdown: true,
      child: DropdownButtonHideUnderline(
        child: DropdownButton<T>(
          isExpanded: true,
          icon: widget.icon != null ? Icon(widget.icon) : null,
          iconEnabledColor: Theme.of(context).iconTheme.color,
          iconSize: ThemeSizes.icon[Sizes.M],
          onChanged: _onChanged,
          value: _selected ?? widget.defaultValue ?? widget.keyValues.keys.first,
          items: _typeSelectItems,
      ))
    );
  }
}