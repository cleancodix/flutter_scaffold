
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';

import 'custom.text.input.dart';

class PasswordTextInput extends StatelessWidget {

  PasswordTextInput({ model, this.testKey })
      : this.model = model ?? PasswordTextInputModel();

  final PasswordTextInputModel model;
  final String testKey;

  @override
  Widget build(BuildContext context) {
    return CustomTextInput(model: model, testKey: testKey);
  }
}

class PasswordTextInputModel extends CustomTextInputModel {

  PasswordTextInputModel({String value, dynamic next}) : super(value: value, next: next);

  int minLength = 4;

  @override
  String get label => translate["Password"];
  @override
  String get invalidMessage => minLength.toString() + " " + translate["characters minimum"];
  @override
  String get errorMessage => translate["Incorrect credentials"];

  final bool secret = true;
  final bool required = true;
  final Icon icon = Icon(Icons.vpn_key, size: ThemeSizes.icon[Sizes.M]);
  final TextAlign textAlign = TextAlign.center;
}