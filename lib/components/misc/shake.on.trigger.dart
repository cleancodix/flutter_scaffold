
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart';

class ShakeOnTrigger extends StatefulWidget {

  ShakeOnTrigger({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  ShakeOnTriggerState createState() => ShakeOnTriggerState();
}

class ShakeOnTriggerState extends State<ShakeOnTrigger> with SingleTickerProviderStateMixin {

  AnimationController animationController;
  Animation<double> animation;

  @override
  initState() {
    super.initState();

    animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2000),
    )..addListener(() => setState(() {}));

    animation = Tween<double>(
      begin: 50.0,
      end: 80.0,
    ).animate(animationController);
  }

  Matrix4 _shake() {
    double progress = animationController.value;
    double offset = sin(progress * pi * 10.0);
    return Matrix4.translation(Vector3(offset * 4, 0.0, 0.0));
  }

  // Call this method to trigger animation
  void animate() {
    animationController.reset();
    animationController.forward();
  }

  @override
  dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Transform(
    transform: _shake(),
    child: widget.child,
  );
}