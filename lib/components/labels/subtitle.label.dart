
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';

import 'custom.label.dart';

class SubtitleLabel extends CustomLabel {

  SubtitleLabel(text, {
    heroTag,
    maxLines = 1,
    expand = false,
    align,
    color,
    size,
    overflow = TextOverflow.visible,
    wrap = true,
    testKey
  }) : super(text,
      heroTag: heroTag,
      maxLines: maxLines,
      expand: expand,
      align: align,
      color: color,
      size: size,
      overflow: overflow,
      wrap: wrap,
      testKey: testKey
  );

  @override
  TextStyle getTextStyle(BuildContext context) {
    switch(size) {
      case Sizes.M: return Theme.of(context).textTheme.subtitle1;
      case Sizes.S: return Theme.of(context).textTheme.subtitle2;
      default: return Theme.of(context).textTheme.subtitle1;
    }
  }
}