

import 'package:flutterscaffold/components/buttons/simple.button.dart';
import 'package:flutter/material.dart';

class OkButton extends StatelessWidget {

  OkButton({
    this.action,
    this.testKey,
    this.hero,
    this.color,
    this.padding,
    this.disabled = false,
  });

  final Function action;
  final String testKey;
  final String hero;
  final Color color;
  final EdgeInsets padding;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return SimpleButton(
      testKey: testKey,
      action: action,
      color: color,
      hero: hero,
      padding: padding,
      disabled: disabled,
      child: Icon(Icons.check)
    );
  }
}