
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/widget.dart';

class SimpleButton extends StatelessWidget {

  final Function action;
  final Widget child;
  final String testKey;
  final Color color;
  final Color disabledColor;
  final ShapeBorder shape;
  final bool expand;
  final bool constrained;
  final bool disabled;
  final String hero;
  final EdgeInsets padding;

  SimpleButton({
    this.action,
    this.padding,
    this.child,
    this.shape,
    this.testKey,
    this.color,
    this.disabledColor,
    this.hero,
    this.expand = false,
    this.constrained = true,
    this.disabled = false,
  });

  Widget get mainContent =>
      constrained ? Stack(
        alignment: Alignment.center,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: ThemeSizes.buttonHeight[Sizes.M],
              maxHeight: ThemeSizes.buttonHeight[Sizes.M],
              minWidth: ThemeSizes.buttonWidth[Sizes.M] - ThemeSizes.paddingValue[Sizes.M] * 2,
            ),
            child: expand ? Center(child: child) : null,
          ),
          ...(!expand ? [child] : [])
        ],
      ) : expand ? Center(child: child) : child;

  @override
  Widget build(BuildContext context) {
    // ignore: deprecated_member_use
    return WidgetTool.wrapWithHero(hero, RaisedButton(
        key: Key(testKey),
        padding: padding ?? ThemeSizes.paddingSymmetric[Sizes.M][Sizes.Zero],
        onPressed: !disabled ? action : null,
        disabledColor: disabledColor ?? Theme.of(context).disabledColor,
        color: color ?? Theme.of(context).buttonColor,
        shape: shape ?? RoundedRectangleBorder(borderRadius: ThemeSizes.borderRadius[Sizes.XS]),
        child: mainContent,
      ),
    );
  }
}