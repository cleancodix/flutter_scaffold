
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/layout.dart';
import 'app.bar.button.dart';

class MenuButton extends AppBarButton {

  MenuButton({Future Function() onMenu}) : super(
      icon: Icons.menu,
      hero: "MENU_BUTTON",
      testKey: TestKey(key: Keys.MenuButton),
      action: (BuildContext context) async {
        if (onMenu != null) await onMenu();
        LayoutService(context).get.scaffold.openDrawer();
      },
  );
}