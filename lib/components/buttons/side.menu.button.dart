
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/services/layout.dart';

class SideMenuButton extends StatelessWidget {

  SideMenuButton({
    this.action,
    this.testKey,
    this.title,
    this.icon,
  });

  final Function action;
  final String testKey;
  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return  ListTile(
      key: Key(testKey ?? TestKey(key: Keys.SideMenuButton, id: title)),
      title: SubtitleLabel(title),
      minLeadingWidth: ThemeSizes.paddingValue[Sizes.S],
      leading: Icon(
          icon ?? Icons.arrow_forward_ios,
          color: Theme.of(context).iconTheme.color
      ),
      onTap: () {
        action();
        LayoutService(context).get.toggleSideMenu();
      },
    );
  }
}