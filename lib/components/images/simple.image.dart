
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';

class SimpleImage extends StatelessWidget {

  final Axis axis;
  final Sizes size;
  final double height;
  final double width;
  final String path;
  final String hero;

  SimpleImage({
    key,
    this.size,
    this.height,
    this.width,
    this.path,
    this.hero,
    this.axis
  });

  double get _size => ThemeSizes.image[size ?? Sizes.M];
  double get _height => height ?? _size;
  double get _width => width ?? _size;

  @override
  Widget build(BuildContext context) {

    return Container(
          height: _height,
          width: _width,
          child: Center(
            child: Hero(
                tag: hero,
                child: Image.asset(
                    path,
                    height: _height,
                    width: _width,
                )
            ),
          )
    );
  }
}