
import 'package:easter_egg_trigger/easter_egg_trigger.dart';
import 'package:flutterscaffold/components/images/simple.image.dart';
import 'package:flutterscaffold/components/misc/rotate.on.loading.dart';
import 'package:flutterscaffold/constants/config/assets.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/services/theme.dart';

class MainLogo extends StatefulWidget {

  final Axis axis;
  final String hero;
  final Sizes size;
  final double height;
  final double width;

  MainLogo({
    key,
    this.size,
    this.axis,
    this.hero = "MAIN_LOGO"
  }) :
        height = ThemeSizes.image[size ?? Sizes.M],
        width = ThemeSizes.image[size ?? Sizes.M],
        super(key: key);

  @override
  MainLogoState createState() => MainLogoState();
}

class MainLogoState extends State<MainLogo> {

  String path = Assets[Logos.Vertical];

  bool get isSurprise => path == Assets[Logos.EasterEgg];

  void _surprise() => setState(() {
    path = Assets[Logos.EasterEgg];
  });

  void _setPath(BuildContext context) {
    bool isDark = ThemeService(context).get.isDark;
    switch(widget.axis) {
      case Axis.vertical: path = isDark ? Assets[Logos.VerticalDark] : Assets[Logos.Vertical];
      break;
      case Axis.horizontal: path = isDark ? Assets[Logos.HorizontalDark] : Assets[Logos.Horizontal];
      break;
      default: path = Assets[Logos.Horizontal];
    }
  }

  Widget _wrapWithRotateIfSurprise(Widget child) => isSurprise ?
      RotateOnLoading(child: child, force: true) : child;

  @override
  Widget build(BuildContext context) {
    if (!isSurprise) _setPath(context);

    Widget _image = Container(
      color: isSurprise ? null : Theme.of(context).primaryColor,
      child: SimpleImage(
        path: path,
        hero: widget.hero,
        height: widget.height,
        width: widget.width,
      ),
    );

    return EasterEggTrigger(
      action: _surprise,
      child: _wrapWithRotateIfSurprise(_image)
    );
  }
}