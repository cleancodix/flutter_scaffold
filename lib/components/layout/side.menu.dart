
import 'package:flutterscaffold/components/buttons/side.menu.button.dart';
import 'package:flutterscaffold/constants/config/assets.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/layout.dart';
import 'package:flutterscaffold/services/logout.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:flutterscaffold/services/translation.dart';

class SideMenu extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    LayoutService(context).listen;
    Map<String, String> translate = TranslationService(context).get;
    RouteService routeService = RouteService(context).get;

    return ClipRRect(
      borderRadius: ThemeSizes.borderRadiusHorizontal[Sizes.Zero][Sizes.L],
      child: Drawer(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: SafeArea(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Padding(
                  padding: ThemeSizes.padding[Sizes.S],
                  child: DrawerHeader(
                    child: Container(),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.contain,
                        image: AssetImage(Assets[Logos.Horizontal])
                      )
                    ),
                  ),
                ),
                ...CacheService(context).get.pixabayQueries.map((query) => SideMenuButton(
                  title: query,
                  action: () => routeService.get.goTo(Routes.Pixabay[query]),
                )).toList(),
                SideMenuButton(
                  testKey: TestKey(key: Keys.SideMenuButton, key2: Keys.SettingsButton),
                  title: translate['Settings'],
                  icon: Icons.settings,
                  action: () => routeService.get.goTo(Routes.Settings),
                ),
                SideMenuButton(
                  testKey: TestKey(key: Keys.SideMenuButton, key2: Keys.LogOutButton),
                  title: translate['Log out'],
                  icon: Icons.directions_run,
                  action: () => LogoutService.exit(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}