import 'package:flutterscaffold/components/buttons/menu.button.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';

import '../buttons/custom.back.button.dart';
class CustomSliverBar extends StatelessWidget {
  final Widget title;
  final Widget background;
  final Widget collapsed;
  final double expandedHeight;
  final EdgeInsets padding;
  final BorderRadius borderRadius;
  final Future Function() onBack;
  final Future Function() onMenu;

  CustomSliverBar({
    this.title,
    this.background,
    this.collapsed,
    this.expandedHeight,
    this.padding,
    this.borderRadius,
    this.onBack,
    this.onMenu,
  });

  @override
  Widget build(BuildContext context) {
    bool canPop = Navigator.of(context).canPop();

    Widget mainContent = collapsed != null ? title != null ?
    Stack(children: [collapsed, title]) : collapsed : title;

    CustomBackButton backButton = canPop ?
      CustomBackButton(onBack: onBack) : null;

    return SliverAppBar(
      pinned: true,
      expandedHeight: expandedHeight,
      automaticallyImplyLeading: false,
      leading: backButton,
      leadingWidth: backButton?.width ?? 0,
      actions: [MenuButton(onMenu: onMenu)],
      flexibleSpace: ClipRRect(
        child: FlexibleSpaceBar(
          centerTitle: true,
          title: mainContent,
          background: background,
          titlePadding: padding,
        ),
        borderRadius: borderRadius ?? ThemeSizes.borderRadiusVertical[Sizes.Zero][Sizes.L],
      ),
      shape: RoundedRectangleBorder(
        borderRadius: borderRadius ?? ThemeSizes.borderRadiusVertical[Sizes.Zero][Sizes.L],
      ),
    );
  }
}