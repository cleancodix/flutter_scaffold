import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/log.dart';

class SliverView extends StatelessWidget {

  final Key key;
  final Widget child;
  final Widget appBar;
  final bool fillRemaining;
  final bool overlap;
  final bool nested;
  final ScrollPhysics physics;
  final List<Widget> slivers;
  final Function onRefresh;
  final GlobalKey<RefreshIndicatorState> refreshKey;

  SliverView({
    this.key,
    this.child,
    this.appBar,
    this.fillRemaining = false,
    this.overlap = false,
    this.nested = false,
    this.physics,
    this.slivers,
    this.onRefresh,
    this.refreshKey
  });

  Widget wrapWithRefresh(BuildContext context, { Widget child }) => onRefresh != null ? RefreshIndicator(
      key: refreshKey,
      displacement: MediaQuery.of(context).size.width / 2,
      child: child,
      onRefresh: onRefresh
  ) : child;

  List<Widget> injectOverlap(BuildContext context) {
    List<Widget> result = [];
    try {
      result.add(SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)));
    } catch(e) {
      Log.print("NestedScrollView's context not found", title: "SliverView's overlap fct");
    }
    return result;
  }

  Widget getAbsorber(BuildContext context) => SliverOverlapAbsorber(
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: appBar
  );

  List<Widget> getSlivers(BuildContext context, [bool innerBoxIsScrolled]) => [
    if (overlap) ...injectOverlap(context),
    if (nested && appBar != null) getAbsorber(context),
    if (!nested && appBar != null) appBar,
    if (slivers != null && slivers.length > 0) ...slivers,
    if (!nested && child != null && !fillRemaining) SliverToBoxAdapter(child: child),
    if (!nested && child != null && fillRemaining) SliverFillRemaining(child: child),
  ];

  @override
  Widget build(BuildContext context) {

    return wrapWithRefresh(context,
      child: nested ?
        NestedScrollView(
          key: key,
          physics: physics,
          headerSliverBuilder: getSlivers,
          body: child
        )
      : CustomScrollView(
          key: key,
          physics: physics,
          slivers: getSlivers(context),
        ),
    );
  }
}
