# flutterscaffold

A *ready to dev* Flutter scaffold MVC project by *Mr#* based on the plugin **mvcprovider**.

This plugin has been designed to provide you a **guide** to organise your code and to be usable
with *vanilla* Flutter or with plugin(s) of your choice like *Hooks*, *RxDart*, *Bloc*, *Redux*, etc

You will also find homemade **components**, **tools** and **services** that can save you time
by reusing (some of) them but it depends on your project :)

This project will show you how to use, organize & handle :

    - Logging IN/OUT
    - Translations
    - Http requests
    - Cache data
    - Cache request results
    - Errors
    - Loading events
    - Local storage
    - Dynamic theming
    - Mocking data
    - Routing between views/pages
    - Dynamic routing
    - Securing routing (guards)
    - Retrieving data from an api (Pixabay)
    - Sliver animations (header app bar)
    - Hero animations
    - Driven tests
    
If this scaffold helped you to learn and/or starting your projects faster or if you're simply happy
please give a ***like*** to my plugin <a href="https://pub.dev/packages/mvcprovider" target="_blank">mvcprovider</a>
    
## Getting Started

To run the project, please install first :

    - Android Studio
    - Flutter
    
Then launch an emulator or connect your phone with an USB cable
and click on run in Android Studio.

Tog


## Start a new app based on this project

Please install first :

    - Android Studio
    - Flutter
    
Then create a **new Flutter project** with Android Studio and please *remember*
the app name provided because you will *need it* for next step
then **replace** these files/folders :

    - /assets
    - /lib
    - /test_driver
    - /pubspec.yaml
    
**With those** of the Flutter scaffold. Then run the command :

    - flutter pub get
    

## App name & id

Replace the **app name** at the very first line of the */pubspec.yaml* file
with the app name *that you provided* during Flutter project creation.

You will to need to replace all imports
**Hint :** Do Ctrl + shift + R to open the global *search & replace* window.
And look for: *import 'package:flutterscaffold/*
Then replace all with *import 'package:yourappname/*

Next, find these lines into your */pubspec.yaml* file : 

```
flutter_launcher_name:
  name: "Flutter Scaffold"
```

Replace *Flutter Scaffold* with the name you want to display on your app.
And run the command :

    - flutter pub run flutter_launcher_name:main
    
    
## Icon & splashscreen

In order to add your own app icon & splashscreen
you just have to replace *icon.png* and *splash.png*
in the *assets/launcher* folder with your files.

Then run these commands :

    - flutter pub run flutter_launcher_icons:main
    - flutter pub pub run flutter_native_splash:create

    
## To finish

Then you can run this command if app name or icon didn't change after running the app :

    - flutter clean
