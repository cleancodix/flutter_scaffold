
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/models/user.data.dart';

class TestDataModel {

  int step = 0;
  final FlutterDriver driver;
  final UserDataModel user;

  TestDataModel([this.driver, this.user]);

  Future<void> _reset() async {
    print("${step++} - Reset the view (to remove previous test error messages or animations)");
    await driver.requestData('restart');
  }

  Future<void> restart([bool _login = true]) async {
    try {
      await driver.waitFor(find.byType('LoginPage'), timeout: Duration(seconds: 2));
    } catch(e) {
      await logout();
    }
    if (_login) await login();
  }

  Future<void> login([bool rememberMe = false]) async {

    await _reset();
    try {
      await driver.waitFor(find.text('Remember me'), timeout: Duration(seconds: 2));
    } catch(e) {
      await restart();
    }

    print("${step++} - Ensure that error messages aren't displayed");
    await driver.waitForAbsent(find.byValueKey(TestKey(feature: Features.Login, key: Keys.EmailTextInput, key2: Keys.ErrorLabel)));
    await driver.waitForAbsent(find.byValueKey(TestKey(feature: Features.Login, key: Keys.PasswordTextInput, key2: Keys.ErrorLabel)));

      print("${step++} - Input email");
      try {
        await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.EmailTextInput)), timeout: Duration(seconds: 2));
        await driver.enterText(user.email, timeout: Duration(seconds: 2));
        await driver.waitFor(find.text(user.email), timeout: Duration(seconds: 2));
      } catch(e) {
        await restart();
      }

      print("${step++} - Input password");
      try {
        await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.PasswordTextInput)), timeout: Duration(seconds: 2));
        await driver.enterText(user.password, timeout: Duration(seconds: 2));
        await driver.waitFor(find.text(user.password), timeout: Duration(seconds: 2));
      } catch(e) {
        await restart();
      }

    if (rememberMe) {
      print("${step++} - Tap the remember me button");
      await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.RememberMeButton)));
    }

    print("${step++} - Submit the form");
    await driver.waitForAbsent(find.byValueKey(TestKey(key: Keys.BackgroundLoader)));
    await driver.tap(find.byValueKey(TestKey(feature: Features.Login, key: Keys.OkButton)));
    await driver.waitForAbsent(find.byType('LoginPage'));
  }

  Future<void> tapMenuButton() async {
    // Scroll up by 1000px in case the menu button isn't visible
    await driver.scrollIntoView(
      find.byValueKey(TestKey(key: Keys.MenuButton)),
      alignment: 1000.0,
    );
    await driver.tap(find.byValueKey(TestKey(key: Keys.MenuButton)));
  }

  Future<void> logout() async {
    print("${step++} - Logout");
    await tapMenuButton();
    await driver.tap(find.byValueKey(TestKey(key: Keys.SideMenuButton, key2: Keys.LogOutButton)));
    await checkPage('LoginPage');
  }

  Future<void> back() async {
    print("${step++} - Tap Back button");
    await driver.tap(find.byValueKey(TestKey(key: Keys.BackButton)));
  }

  Future<void> ensureIsLoading({Duration duration, Future Function() checksDuringLoading}) async {
    print("${step++} - Ensure background loading animation is displayed");
    driver.runUnsynchronized(() async {
      print("${step++} - Ensure background loading animation is displayed");
      await driver.waitFor(find.byValueKey(TestKey(key: Keys.BackgroundLoader)));
      if (checksDuringLoading != null) await checksDuringLoading();
    });
    if (duration != null) await Future.delayed(duration);
    print("${step++} - Ensure background loading animation stopped");
    await driver.waitForAbsent(find.byValueKey(TestKey(key: Keys.BackgroundLoader)));
  }

  Future<void> checkPage(String widgetName) async {
    print("${step++} - Ensure we are on $widgetName");
    await driver.waitFor(find.byType(widgetName));
  }

  Future<void> tryAgain(Future<void> Function(int) todo, {int attempts = 1, Function notPassedCallback}) async {
    int i = 0;
    bool passed = false;
      await Future.forEach(List.filled(attempts, null), (_) async {
        try {
          if (!passed) {
            await todo(i);
            passed = true;
          }
        } catch(e) {
          i++; // Try again :)
        }
      });
      if (!passed && notPassedCallback != null) notPassedCallback();
  }
}
